package com.example.demo;

public class Main {

    public static void main(String[] args) {
        // Execute Authentication Service
        AuthenticationService authService = new AuthenticationService();
        System.out.println("Testing AuthenticationService:");
        for (int i = 0; i < 7; i++) {
            authService.login("admin", "wrongpassword");
        }

        // Execute Registration Service
        RegistrationService registrationService = new RegistrationService();
        System.out.println("\nTesting RegistrationService:");
        registrationService.registerSimpleUser("user1", "weakpass");

        LdapAuthenticationService auth = new LdapAuthenticationService();
        auth.authenticateUser();
    }
}
