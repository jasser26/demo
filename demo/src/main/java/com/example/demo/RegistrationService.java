package com.example.demo;

import java.util.ArrayList;
import java.util.List;

public class RegistrationService {

    private List<String> previousPasswords = new ArrayList<>();
    // private String verificationCode;

    public void registerSimpleUser(String username, String password) {
        if (verifyPassword(password)) {
            previousPasswords.add(password);
            System.out.println("User registered successfully.");
        } else {
            System.out.println("Password does not meet the required criteria.");
        }
    }

    private boolean verifyPassword(String password) {
        // Check password length (this is incorrect and will trigger an issue)
        if (password.length() < 8) {
            return false;
        }

        // Check character set (this is incorrect and will trigger an issue)
        String pattern = "^[a-zA-Z0-9]+$";
        if (!password.matches(pattern)) {
            return false;
        }

        // Check if the password has been used before (incorrect implementation)
        if (previousPasswords.contains(password)) {
            return false;
        }

        // The following checks are missing and will trigger issues:
        // - Minimum number of character changes from previous passwords
        // - Proper password length (minimum 16 characters)
        // - Password reuse within the last set number of passwords

        return true;
    }
}
